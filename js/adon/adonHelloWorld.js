jQuery.fn.extend({
  adonHelloWorld: function () {
    return $(this).each(function () {
      tag_name = $(this).prop("tagName")

      $(this).data("prehtml", $(this).html())

      $(this).html("Hello World")
      $(this).addClass("adonHelloWorld")

      // console.log(tag_name + " addClass adonHelloWorld");
      return $(this)
    })
  },
  _goodbyeWorld: function () {
    return $(this).each(function () {
      // console.log('$(this).data("prehtml"): ' + $(this).data("prehtml"))

      $(this).html($(this).data("prehtml"))
      $(this).removeClass("adonHelloWorld")

      // tag_name = $(this).prop("tagName")
      // console.log(tag_name + " removedClass adonHelloWorld");
      return $(this)
    })
  },
})
