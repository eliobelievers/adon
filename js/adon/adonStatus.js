jQuery.fn.extend({
  adonStatus: function (val) {
    if (!val) return $(this).data("status")
    $(this).removeClass("adonStatus-" + $(this).data("status"))
    $(this).data("status", val)
    $(this).addClass("adonStatus-" + val)
    return val
  },
  adonIsDisabled: function () {
    return $(this).each(function () {
      $(this).adonHasStatus("disabled") || $(this).adonHasStatus("error")
    })
  },
  adonDisable: function () {
    return $(this).each(function () {
      return $(this).adonStatus("disabled")
    })
  },
  adonRaise: function () {
    return $(this).each(function () {
      return $(this).adonStatus("error")
    })
  },
  adonEnable: function () {
    return $(this).each(function () {
      return $(this).adonStatus("enabled")
    })
  },
  adonHasStatus: function (val) {
    return $(this).hasClass("adonStatus-" + val)
  },
  _adofStatus: function () {
    return $(this).each(function () {
      $(this).removeClass("adonStatus-" + $(this).data("status"))
    })
  },
})
