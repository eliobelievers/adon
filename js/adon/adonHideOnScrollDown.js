jQuery.fn.extend({
  adonHideOnScrollDown: function () {
    tags = $(this)
    position = $(window).scrollTop()
    tags.each(function (tag) {
      $(this).addClass("adonHideOnScrollDown")
      $(this).mouseover(function () {
        $(this).addClass("adonHover")
      })
      $(this).mouseout(function () {
        $(this).removeClass("adonHover")
      })
    })
    $(window).scroll(function () {
      tags.each(function (tag) {
        if ($(window).scrollTop() > position) {
          $(this)._adonHideIfNotHovering()
        } else {
          $(this).removeClass("adonHide")
        }
      })
      // reset the position after hiding all tags
      position = $(window).scrollTop()
    })
    setInterval(function () {
      curr_position = $(window).scrollTop()
      if (curr_position === position) {
        tags.each(function (tag) {
          $(this)._adonHideIfNotHovering()
        })
      }
    }, 2000)
  },
  _adonHideIfNotHovering: function () {
    if (!$(this).hasClass("adonHover")) $(this).addClass("adonHide")
  },
})
