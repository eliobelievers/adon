const suites = require("../suites/adonTesterSuite")

var testTag = "h1"
var testML = `<${testTag}>PRETEST</${testTag}>`

suites.AdonTesterSuite(
  "jQuery.fn.extend.adonMadStuff",
  "../../js/adon/adonMadStuff.js",
  testML,
  function () {
    it("does all kinds of mad stuff", done => {
      $(testTag).adonMadStuff(true)
      $(testTag)[0].should.have.class("adonMadStuff")
      $(testTag)[0].should.have.class("adonMadStuffCol")
      $(testTag)[0].should.not.have.class("madactive")
      $(testTag)._adofMadStuff()
      $(testTag)[0].should.not.have.class("adonMadStuff")
      $(testTag)[0].should.not.have.class("adonMadStuffCol")
      $(testTag)[0].should.not.have.class("madactive")
      done()
    })

    it("does color mad stuff only", done => {
      $(testTag).adonMadStuff(false)
      $(testTag)[0].should.not.have.class("adonMadStuff")
      $(testTag)[0].should.have.class("adonMadStuffCol")
      $(testTag)[0].should.not.have.class("madactive")
      $(testTag)._adofMadStuff()
      $(testTag)[0].should.not.have.class("adonMadStuff")
      $(testTag)[0].should.not.have.class("adonMadStuffCol")
      $(testTag)[0].should.not.have.class("madactive")
      done()
    })
  }
)
