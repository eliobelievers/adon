const suites = require("../suites/adonTesterSuite")

var testTag = "h1"
var testML = `<${testTag}>PRETEST</${testTag}>`

suites.AdonTesterSuite(
  "jQuery.fn.extend.adonStatus",
  "../../js/adon/adonStatus.js",
  testML,
  function () {
    it("adonStatus", done => {
      $(testTag).adonStatus("testing")
      $(testTag)[0].should.have.class("adonStatus-testing")
      $(testTag).adonStatus().should.equal("testing")
      $(testTag)._adofStatus()
      $(testTag)[0].className.should.equal("")
      done()
    })
    it("adonStatus usage", done => {
      $(testTag).adonStatus("retesting")
      $(testTag)[0].should.have.class("adonStatus-retesting")
      $(testTag).adonStatus().should.equal("retesting")
      $(testTag)._adofStatus()
      done()
    })
    it("adonStatus adonDisable", done => {
      $(testTag).adonDisable()
      $(testTag)[0].should.have.class("adonStatus-disabled")
      $(testTag).adonStatus().should.equal("disabled")
      $(testTag)._adofStatus()
      done()
    })
    it("adonStatus adonRaise", done => {
      $(testTag).adonRaise()
      $(testTag)[0].should.have.class("adonStatus-error")
      $(testTag).adonStatus().should.equal("error")
      $(testTag)._adofStatus()
      done()
    })
    it("adonStatus adonEnable", done => {
      $(testTag).adonEnable()
      $(testTag)[0].should.have.class("adonStatus-enabled")
      $(testTag).adonStatus().should.equal("enabled")
      $(testTag)._adofStatus()
      done()
    })
    //
    it("adonStatus adonHasStatus", done => {
      $(testTag).adonDisable()
      $(testTag)[0].should.have.class("adonStatus-disabled")
      $(testTag).adonHasStatus("enabled").should.equal(false)
      $(testTag).adonHasStatus("disabled").should.equal(true)
      $(testTag)._adofStatus()
      done()
    })
  }
)
