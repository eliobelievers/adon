# Installing adon

- [adon Prerequisites](/eliosin/adon/prerequisites.html)
- [Installing god](/eliosin/god/installing.html)

## Add it to your compile funnel

Add to your main scss page - i.e. the one which your build tools target:

```scss
@import "../node_modules/@elioway/adon/stylesheets/adon/adonEvent";
@import "../node_modules/@elioway/adon/stylesheets/adon/adonHideOnScrollDown";
```

Then add to your gulp file and the `js_watch` list, any adons you want to use:

```javascript
var js_watch = [
  "./js/plugins.js",
  "./js/adon/adon*.js",
  "../node_modules/@elioway/adon/js/adon/adonEvent.js",
  "../node_modules/@elioway/adon/js/adon/adonHideOnScrollDown.js",
]
```

## Nah! I'll just use the dist

```html
<link
  rel="stylesheet"
  href="node_modules/@elioway/adon/dist/css/adon.min.css"
/>
```

Add the scripts to the bottom of the page:

```
  <script
    src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
    crossorigin="anonymous"
  ></script>
  <script src="node_modules/@elioway/adon/dist/js/adons.js"></script>
  <script src="node_modules/@elioway/adon/dist/js/main.js"></script>
</body>
```
